FROM debian:buster as builder

ARG BASE_VERSION

ENV TERRAFORM_VERSION=$BASE_VERSION

WORKDIR /root/

RUN apt-get update \
 && apt-get install -y wget unzip \
 && wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
 && unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
 && chmod +x terraform

FROM mdeterman/k8-kubectl as terraform

COPY --from=builder /root/terraform /usr/local/bin

RUN apt-get update \
 && apt-get install --no-install-recommends -y ca-certificates ruby \
 && rm -rf /var/cache/apt/archives
